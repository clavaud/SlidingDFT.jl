module SlidingDFT
export SlidingDFT

greetSlidingDFT() = print("Hello World! -from sDFT")
export greetSlidingDFT

mutable struct sdftStruct
	x::Array{ComplexF32}; #Time domain samples are stored in this circular buffer
	x_index::Int32; #index of the next item in the buffer to be used. Equivalently, the number of samples that have been seen so far modulo nbBin
	twiddle::Array{ComplexF32}; #Twiddle factors for the update algorithm
	S::Array{ComplexF32}; #Frequency domain values (unwindowed!)
	dft::Array{ComplexF32}; #Frequency domain values (windowed)
	damping_factor::ComplexF32; #A damping factor introduced into the recursive DFT algorithm to guarantee stability
	data_valid::Bool; #true when nbit are already treated (output init finished)
	nbBin::Int32; #bin size
	r_to_N::ComplexF32;
	meanVal::Int32; #bin size oversample
end
export sdftStruct

function initSdft(nbBin; meanVal = 1)
	nbBin = nbBin*meanVal
	stru = sdftStruct(zeros(ComplexF32,nbBin),
							1,
							zeros(ComplexF32,nbBin),
							zeros(ComplexF32,nbBin),
							zeros(ComplexF32,nbBin),
							1.0-eps(Float32),
							false,
							nbBin,
							0.0,
							meanVal
	);

		# Compute the twiddle factors, and zero the x and S arrays
		factor::ComplexF32 = 0;
		j::ComplexF32 =  1.0im;
		for k = 0:1:nbBin-1
			factor = (2.0 * pi) * k / nbBin
			stru.twiddle[k+1] = exp(j * factor)
		end
		stru.r_to_N = (stru.damping_factor)^nbBin
		stru.x = zeros(ComplexF32,nbBin)
		stru.S = zeros(ComplexF32,nbBin)
		stru.dft = zeros(ComplexF32,nbBin)

		return stru
end
export initSdft

"""
---
This code efficiently computes discrete Fourier transforms (DFT) from a continuous stream
It compute a new DFT to each sample of the input stream by applying a sliding window over the last nbBin samples
Windowing is done using a Hanning window[1]
A damping factor is used to improve stability in the face of numerical rounding errors [2]
[1] E. Jacobsen and R. Lyons, “The Sliding DFT,” IEEE Signal Process. Mag., vol. 20, no. 2, pp. 74–80, Mar. 2003.
[2] E. Jacobsen and R. Lyons, “An Update to the Sliding DFT,” IEEE Signal Process. Mag., vol. 21, no. 1, pp. 110-111, 2004.

# --- Syntax
sDFT(sig, stru::sdftStruct)
# --- Input parameters
- sig		 : Signal [Array{Complex{Float32}}]

# --- Output parameters
- dftAll	  : return one vector of nbBin complexe for each input sample [Array{Complex{Float32}},2]
# ---
# v 2.0 - Corentin Lavaud
"""
function sDFT!(stru::sdftStruct, sig::Union{ComplexF32,Float32})

	#Update the storage of the time domain values
	old_x = stru.x[stru.x_index]
	stru.x[stru.x_index] = sig

	# Update the DFT
	# temp = stru.r_to_N * old_x + sig
	for k = 1:1:stru.nbBin
		stru.S[k] = stru.twiddle[k] * (stru.damping_factor * stru.S[k] - stru.r_to_N * old_x + sig)
		stru.dft[k]=stru.S[k]
	end

	# Apply the Hanning window
	stru.dft[1] = 0.5*stru.S[1] - 0.25*(stru.S[stru.nbBin] + stru.S[2])
	for k = 2:1:(stru.nbBin-1)
		stru.dft[k] = 0.5*stru.S[k] - 0.25*(stru.S[k - 1] + stru.S[k + 1])
	end
	stru.dft[stru.nbBin] = 0.5*stru.S[stru.nbBin] - 0.25*(stru.S[stru.nbBin - 1] + stru.S[1]);

	# Increment the counter
	stru.x_index+=1;
	if (stru.x_index > stru.nbBin)
		stru.data_valid = true;
		stru.x_index = 1;
	end

	# return temp = 20*log10.(abs2.(dft[1:Int(nbBin/2)]).+eps(Float32))

	if stru.meanVal > 1 #over bin
		nFFT = Int(stru.nbBin/stru.meanVal)
		temp = zeros(ComplexF32, nFFT)
		delta = Int(round(stru.meanVal/2))
		for ii = 2:1:nFFT-1
			temp[ii] = mean(stru.dft[ii*stru.meanVal-delta:ii*stru.meanVal+delta])
		end
		temp[1]= mean(stru.dft[1:1*stru.meanVal+delta])
		temp[end]= mean(stru.dft[nFFT*stru.meanVal-delta:end])
		return temp
	else
		return stru.dft
	end
end
export sDFT!

end # module
